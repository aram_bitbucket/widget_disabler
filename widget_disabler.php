<?php
/**
Plugin Name: Disable widget on the fly
Version: 1.0.0
Description: A plugin that disables widgets on the fly
*/

function disabler_add_fields($widget, $return, $instance){
    ?>
    <p><input class = "widefat" type = "checkbox" <?php checked((isset($instance['disable_widget']) && $instance['disable_widget']) ? $instance['disable_widget'] : 0); ?> id = "<?php echo $widget->get_field_id('disable_widget'); ?>" name = "<?php echo $widget->get_field_name('disable_widget'); ?>">&nbsp;<label for = "<?php echo $widget->get_field_id('disable_widget'); ?>"><?php _e('Disable Widget'); ?></label></p>
    <p><input class = "widefat" type = "checkbox" <?php checked((isset($instance['disable_widget_title']) && $instance['disable_widget_title']) ? $instance['disable_widget_title'] : 0); ?> id = "<?php echo $widget->get_field_id('disable_widget_title'); ?>" name = "<?php echo $widget->get_field_name('disable_widget_title'); ?>">&nbsp;<label for = "<?php echo $widget->get_field_id('disable_widget_title'); ?>"><?php _e('Disable Widget Title'); ?></label></p>
<?php
    return $return;
}

function disabler_update_values($instance, $new_instance, $old_instance, $widget){
    $instance['disable_widget'] = ! empty( $new_instance['disable_widget'] );
    $instance['disable_widget_title'] = ! empty( $new_instance['disable_widget_title'] );
    return $instance;
}

function disabler_display_values($instance,$widget,$args){
    if (isset($instance['disable_widget']) && $instance['disable_widget']){
        return false;
    }
    else if (isset($instance['disable_widget_title']) && $instance['disable_widget_title']){
        add_action('widget_title', '__return_false');
    }
    return $instance;
}

function disabler_render_form($instance, $widget){
    if (isset($instance['disable_widget']) && $instance['disable_widget']){
        echo "<script>jQuery('.widget[id*=".$widget->id."] .widget-top').addClass('disabled-widget');
        jQuery('.widget[id*=".$widget->id."] .widget-title h4').prepend('<span class = \'disable_message\'>Disabled </span>');</script>";
    }
    else{
        echo "<script>
            jQuery('.widget[id*=".$widget->id."] .widget-top').removeClass('disabled-widget');
            jQuery('.widget[id*=".$widget->id."] .widget-title h4 .disable_message').remove();
        </script>";
    }
    return $instance;
}

function disabler_plugin_stylesheets(){
    wp_enqueue_style( 'style', plugin_dir_url( __FILE__ )."/style.css" );
}




add_action('in_widget_form','disabler_add_fields', null, 3);
add_action('widget_update_callback','disabler_update_values',null,4);
add_action('widget_display_callback', 'disabler_display_values',null,3);
add_action('widget_form_callback','disabler_render_form',null,2);
add_action( 'admin_enqueue_scripts', 'disabler_plugin_stylesheets' );
